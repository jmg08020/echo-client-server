all: echo-server echo-client

echo-server:
	g++ -o echo-server server.cpp -lpthread

echo-client:
	g++ -o echo-client client.cpp -lpthread

clean:
	rm -f echo-server
	rm -f echo-client
	rm -f *.o
